<?php
/**
 * Plugin Name: WooCommerce AtPos Payment Gateway
 * Plugin URI: https://bitbucket.org/pad52/woocommerce-atpos/
 * Description: Add AtPos Payment Gateways for WooCommerce (@POS in a product of SIA). 
 * Author: pad
 * Author URI: https://bitbucket.org/pad52
 * Author e-mail: paolo.arnaldo@gmail.com
 * Version: 1.0.0
 * License: GPLv2 or later
 * Tags: WooCommerce, AtPos, @pos, SIA
 *
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 *
 */


/**
 * TODO
 *
 * - Cancel order if user do url_back
 * 
 *
 */

if ( ! defined( 'ABSPATH' ) )
	exit;

add_action('plugins_loaded', 'init_atpos');

function init_atpos(){
  
  function add_atpos_gateway_class( $methods ) {
	$methods[] = 'WC_Gateway_AtPos'; 
	return $methods;
  }

  add_filter( 'woocommerce_payment_gateways', 'add_atpos_gateway_class' );

  
  if(class_exists('WC_Payment_Gateway')){
	
	class WC_Gateway_AtPos extends WC_Payment_Gateway {

	/** @var boolean Whether or not logging is enabled */
	public static $log_enabled = false;

	/** @var WC_Logger Logger instance */
	public static $log = false;
	  
	public function __construct(){
		$this->id               = 'atpos';
		$this->icon             = apply_filters( 'woocommerce_gateway_icon', plugins_url( 'assets/creditCards.png' , __FILE__ ) );
		$this->has_fields       = false; 
		$this->notify_url        	= WC()->api_request_url( 'WC_Gateway_AtPos' );
		$this->method_title     = 'AtPos';	
		$this->method_description     = 'AtPos payment method';				
		$this->init_form_fields();
		$this->init_settings();

		$this->title              	  = $this->get_option( 'title' );
		$this->description            = $this->get_option( 'description' );

		$this->testmode       	= 'yes' === $this->get_option( 'testmode', 'no' );
		$this->apName 			= $this->get_option( 'apName' );
		$this->apPos    		= $this->get_option( 'apPos' );
		$this->apMerchantId  	= $this->get_option( 'apMerchantId' );
		$this->apMac       		= $this->get_option( 'apMac' );
		$this->apMacEsito       = $this->get_option( 'apMacEsito' );
		
		self::$log_enabled    	= $this->testmode;

		
		add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );

		// Payment listener/API hook
		// This enables IPN payment system
		add_action( 'woocommerce_api_wc_gateway_atpos', array( $this, 'check_response_ipn' ) );


		//add_action( 'woocommerce_api_wc_gateway_atpos_done', array( $this, 'check_response_done' ) );
		//add_action( 'woocommerce_api_wc_gateway_atpos_back', array( $this, 'check_response_back' ) );
		
		//Helper action
		add_action( 'valid-atpos-ipn-request', array( $this, 'valid_response' ) );
	}

	/**
	* Logging method
	* @param  string $message
	*/
	public static function log( $message ) {
		if ( self::$log_enabled ) {
			if ( empty( self::$log ) ) {
				self::$log = new WC_Logger();
			}
			self::$log->add( 'atpos', $message );
		}
	}

	protected function get_atpos_order( $order_id ) {
		if (  empty($order_id) ){
			WC_Gateway_AtPos::log( 'Error: Order ID were not found in "NUMORD".' );
			return false;
		}

		if($this->testmode)
			//$order_id = ltrim( wc_clean( $posted['NUMORD'] ),$this->apName ); //Sandbox order key fix
		$order = wc_get_order( $order_id );
		
		if ( ! is_object( $order ) ) {
			WC_Gateway_AtPos::log( 'Error: Order Keys do not match.' );
			return false;
		}

		return $order;
	}
	  
	public function admin_options(){
		?>
		<h3><?php _e( 'AtPos', 'woocommerce' ); ?></h3>
		<p><?php _e( '@POS (AtPos) is a Payment gateway from SIA s.p.a.', 'woocommerce' ); ?></p>
		<table class="form-table">
			  <?php $this->generate_settings_html(); ?>
		</table>
		<?php
	}
	  

	public function init_form_fields(){
		$this->form_fields = array(
			'enabled' => array(
			  'title' => __( 'Enable/Disable', 'woocommerce' ),
			  'type' => 'checkbox',
			  'label' => __( 'Enable AtPos', 'woocommerce' ),
			  'default' => 'yes'
			  ),
			'title' => array(
			  'title' => __( 'Title', 'woocommerce' ),
			  'type' => 'text',
			  'description' => __( 'This controls the title which the user sees during checkout.', 'woocommerce' ),
			  'default' => __( 'AtPos', 'woocommerce' ),
			  'desc_tip'      => true,
			  ),
			'description' => array(
				'title' => __( 'Customer Message', 'woocommerce' ),
				'type' => 'textarea',
				'default' => ''
			  ),
			
			'apPos' => array(
			  'title' => __( 'apPos Key', 'woocommerce' ),
			  'type' => 'text',
			  'description' => __( 'This controls the order key associated to your @pos account on SIA servers', 'woocommerce' ),
			  'default' => '',
			  'desc_tip'      => true,
			  'placeholder' => 'Your Private Key'
			  ),
			'apMerchantId' => array(
			  'title' => __( 'apMerchantId Key', 'woocommerce' ),
			  'type' => 'text',
			  'description' => __( 'This controls your apMerchantId associated to your account on SIA servers', 'woocommerce' ),
			  'default' => '',
			  'desc_tip'      => true,
			  'placeholder' => 'Your Secret Merchant Id'
			  ),
			'apMac' => array(
			  'title' => __( 'apMac Key', 'woocommerce' ),
			  'type' => 'text',
			  'description' => __( 'This is the MAC key used for make payment requests associated to your account on SIA servers', 'woocommerce' ),
			  'default' => '',
			  'desc_tip'      => true,
			  'placeholder' => 'Your Secret MAC Key'
			  ),
			'apMacEsito' => array(
			  'title' => __( 'apMacEsito Key', 'woocommerce' ),
			  'type' => 'text',
			  'description' => __( 'This is the MacEsito key used to check the validity of "payment done" requests. The key is associated to your account on SIA servers', 'woocommerce' ),
			  'default' => '',
			  'desc_tip'      => true,
			  'placeholder' => 'Your Secret Key'
			  ),
			'apName' => array(
			  'title' => __( 'apPos Name', 'woocommerce' ),
			  'type' => 'text',
			  'description' => __( 'This take the name of your pos id in the sandbox of SIA test servers', 'woocommerce' ),
			  'default' => 'partybag',
			  'desc_tip'      => true,
			  'placeholder' => 'Your id name in sandbox'
			  ),
			'testmode' => array(
			  'title'       => __( 'AtPos sandbox', 'woocommerce' ),
			  'type'        => 'checkbox',
			  'label'       => __( 'Enable atpos sandbox', 'woocommerce' ),
			  'default'     => 'no',
			  'description' => __( 'atpos sandbox can be used to test payments.', 'woocommerce' )
			)
			/*END*/
			
		  );
	}
	  

	/**
	 * Get AtPos Args for make order payments to AtPos
	**/
	public function get_atpos_args( $order ) {
		global $woocommerce;

		$atpos_args = array(
			//'URLMS'			=> str_replace( 'https:', 'http:', add_query_arg( 'wc-api', 'WC_Gateway_AtPos', home_url( '/' ) ) ),
			'URLMS'			=> WC()->api_request_url( 'WC_Gateway_AtPos' ),
			//'URLDONE'       => esc_url( $this->get_return_url( $order ) ),  //This way we approve only IPN confirmations
			'URLDONE'       => WC()->api_request_url( 'WC_Gateway_AtPos' ), //This way we approve even direct confirmations
			'NUMORD'		=> $order->id,
			'IDNEGOZIO'		=> $this->apMerchantId,
			'IMPORTO' 		=> number_format ( $order->get_total() , 2 , '' , '' ),
			'VALUTA'		=> '978', //! Default currency setting is EURO, one day maybe we can add other in settings panel, even I don't know if SIA servers supports other kind of currency
			'TCONTAB'		=> 'I', //! Default type of is Immedate one day maybe we can add other in settings panel - D for "Differita"
			'TAUTOR'		=> 'I' //! Default type of Autorization is Immedate one day maybe we can add other in settings panel - D for "Differita"
		);

		//Here we calculate the Mac
		$query_string='';
		foreach($atpos_args as $key=>$value)
			$query_string .= $key.'='.$value.'&';
		//Done

		$atpos_args['PAGE'] = 'MASTER';
		$atpos_args['MAC'] = sha1($query_string.$this->apMac);

		$atpos_args['EMAIL'] = $order->billing_email;
		$atpos_args['URLBACK'] = urldecode( $order->get_cancel_order_url() );

		if(get_locale() == 'it_IT' || get_locale() == 'it')
			$atpos_args['LINGUA']='ITA';
		else
			$atpos_args['LINGUA']='EN';
		
		$atpos_args = apply_filters( 'woocommerce_atpos_args', $atpos_args );
		return $atpos_args;
	}

	/**
	 * Get AtPos Url - depends on testmode
	**/
	public function get_request_url( $order, $sandbox = false ) {
  		$atpos_args = http_build_query( $this->get_atpos_args( $order ) );
		if ( $sandbox ) {
			return 'http://atpostest.ssb.it/atpos/pagamenti/main?' . $atpos_args;
		} else {
			return 'https://atpos.ssb.it/atpos/pagamenti/main?' . $atpos_args;
		}
	}
  
	public function process_payment( $order_id ) {
		$order          = wc_get_order( $order_id );
		$atpos_args 	= $this->get_atpos_args( $order );

		return array(
			'result'   => 'success',
			'redirect' => $this->get_request_url( $order, $this->testmode )
		);
	}

		/**
		* CallBack Woocommerce Functions
		* //check_response_done
		* //check_response_back
		* //check_response_ipn
		**/

		/**
	 	* Unused
		**/
		public function check_response_back(){ 
			if ( $this->validate_ipn() ) {
				if ($_SERVER['REQUEST_METHOD'] != 'POST') 
					$posted = wp_unslash( $_GET );
				else
					$posted = wp_unslash( $_POST );

				do_action( "valid-atpos-ipn-request", $posted );
				
			}
			wp_die( "AtPos IPN Request Failure", "AtPos IPN", array( 'response' => 500 ) );
		}

		/**
	 	* Unused
		**/
		public function check_response_done(){ 
			if ( $this->validate_ipn() ) {
				if ($_SERVER['REQUEST_METHOD'] != 'POST') 
					$posted = wp_unslash( $_GET );
				else
					$posted = wp_unslash( $_POST );

				do_action( "valid-atpos-ipn-request", $posted );
				
			}
			wp_die( "AtPos IPN Request Failure", "AtPos IPN", array( 'response' => 500 ) );
		}

		/**
	 	* Atpos IPN check - callback triggered by SIA servers
	 	* similar to paypal IPN system
		**/
		public function check_response_ipn(){ 
			if ( $this->validate_ipn() ) { //It Control the mac and assume validity of the message
				if ($_SERVER['REQUEST_METHOD'] != 'POST') 
					$posted = wp_unslash( $_GET );
				else
					$posted = wp_unslash( $_POST );

				do_action( "valid-atpos-ipn-request", $posted ); //-> do the valid_response($posted) function
				//exit; Now instead of exiting we redirect to payment done page
				//! TODO - We can ask in the settings panel if people wants to validate the payment with IPN only or direct method too.
				// Using exit means that no redirect of users will be made - and we need to modify the URLDONE url too
				// Using wp_redirect instead continues the conversation with the server.. and I hope atpos servers don't scare about it.
				wp_redirect( $this->get_return_url( $order ) );
			}else{
				WC_Gateway_AtPos::log( 'AtPos IPN Request Failure ' . $order->id );
				wp_die( "AtPos IPN Request Failure", "AtPos IPN", array( 'response' => 500 ) );
			}
		}

		/*
		* Set the variables of the order done 
		*/
		public function valid_response( $posted ) {
			if ( ! empty( $posted['NUMORD'] ) && ( $order = $this->get_atpos_order( $posted['NUMORD'] ) ) ) {

				// Lowercase returned variables
				$posted['IDTRANS'] = strtolower( wc_clean( $posted['IDTRANS'] ) );
				$posted['AUT'] = strtolower( wc_clean( $posted['AUT'] ) );

				WC_Gateway_AtPos::log( 'Found order #' . $order->id );
				WC_Gateway_AtPos::log( 'Payment status: ' . wc_clean( $posted['ESITO'] ) );
				
				call_user_func( array( $this, 'payment_status_completed' ), $order, $posted );
			}
		}
		

		protected function payment_status_completed( $order, $posted ) {
			if ( $order->has_status( 'completed' ) ) {
				WC_Gateway_AtPos::log( 'Aborting, Order #' . $order->id . ' is already complete.' );
				return;
			}else{

			/* Maybe we don't need all those validations today - when we have time.. 
			
			$this->validate_transaction_type( $posted['txn_type'] );
			$this->validate_currency( $order, $posted['mc_currency'] );
			$this->validate_amount( $order, $posted['mc_gross'] );
			$this->validate_receiver_email( $order, $posted['receiver_email'] );
			$this->save_atpos_meta_data( $order, $posted );

			*/

				if ( $posted['ESITO'] == '00' ) {
					$this->payment_complete( $order, ( ! empty( $posted['AUT'] ) ? wc_clean( $posted['AUT'] ) : '' ), __( 'IPN payment completed', 'woocommerce' ) . wc_clean( $posted['IDTRANS'] ) );
				
				} else {
					$this->payment_on_hold( $order, sprintf( __( 'Payment pending: %s', 'woocommerce' ), wc_clean( $posted['ESITO'] ) ) );
				}
			}
		}

		protected function payment_complete( $order, $txn_id = '', $note = '' ) {
			$order->add_order_note( $note );
			$order->payment_complete( $txn_id );
			$order->reduce_order_stock();
			WC()->cart->empty_cart();
		}

		/*
		 * Hold order and add note
		 */
		protected function payment_on_hold( $order, $reason = '' ) {
			$order->update_status( 'on-hold', $reason );
			$order->reduce_order_stock();
			WC()->cart->empty_cart();
		}

		public function validate_ipn() {
			@ob_clean();
    		header( 'HTTP/1.1 200 OK' );

			$apVarMac = array('NUMORD','IDNEGOZIO','AUT','IMPORTO','VALUTA','IDTRANS','TCONTAB', 'TAUTOR','ESITO','BPW_TIPO_TRANSAZIONE');
			$apVal = array();

			WC_Gateway_AtPos::log( 'Checking if IPN response is valid' );

			if ($_SERVER['REQUEST_METHOD'] != 'POST') 
				$apVal += wp_unslash( $_GET );
			else
				$apVal += wp_unslash( $_POST );

			//Check MAC validity

			foreach($apVarMac as $key)
				$query_string .= $key.'='.$apVal[$key].'&';
			  	//rtrim($query_string,'&');
			/*
			From the AtPos Documentation
			MAC = Hash_ ( “ NUMORD =<numero d’ordine>&IDNEGOZIO=<merchant id>&AUT=<numero autor>&IMPORTO=<importo> &VALUTA=<valuta>&IDTRANS=<id.transazione>&TCONTAB=<tipo contab>&TAUTOR=<tipo autor>&ESITO=<esito>&BPW_TIPO_TRANSAZIONE =<tipo trans>& BPW_ISSUER_COUNTRY=<nazione dell’issuer>&<stringa segreta esito-API>” )
			*/
			$mioMac = strtoupper( sha1( $query_string . $this->apMacEsito ) );

			if($mioMac != $apVal['MAC']){
				  	WC_Gateway_AtPos::log('Invalid MAC, mine was: '.$mioMac.' I Received: '.$apVal['MAC']);
				  	return false;
			}
			

			// 1. Make sure the payment status is "Completed" 
		    if ( wc_clean( $apVal['ESITO'] ) == '00' )
		    	return true;
		        // simply ignore any IPN that is not completed
		        // Maybe doing this we don't need the payment_hold function
		        // Maybe...
		    
		    return false;
		}


	}
  }
}

