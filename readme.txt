=== WooCommerce AtPos Payment Gateway ===
Contributors: woothemes
Tags: woocommerce, atpos, @pos, sia
Requires at least: 4.2
Tested up to: 4.3
Stable tag: 1.0.0
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Provides integration between SIA S.p.a. @POS (atpos) payment system.

== Description ==

This plugin provides the integration between @POS (AtPos) Payment System and the WooCommerce plugin.

This plugin massively take portions of code from Woocommerce Paypal integrated gateway.

Contributions are welcome via the [BitBucket repository](https://bitbucket.org/pad52/woocommerce-atpos).


== Installation ==

1. Download the plugin file to your computer and unzip it
2. Using an FTP program, or your hosting control panel, upload the unzipped plugin folder to your WordPress installation’s wp-content/plugins/ directory.
3. Activate the plugin from the Plugins menu within the WordPress admin.
4. Configure the plugin in the WooCommerce -> Checkout -> AtPos panel.


== Frequently Asked Questions ==

= Where can I find the setting for this plugin? =

This plugin will add the settings to the Checkout tab, to be found in the WooCommerce > Settings menu.

== Screenshots ==

1. The configuration page


== Changelog ==

= 1.0.0 - 12/09/2015 =

* Initial release
